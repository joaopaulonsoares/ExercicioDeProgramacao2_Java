
package Menus;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Scanner;


public class MostrarImagemPGM {
    //==========================================================================
    public static String le_linha(FileInputStream arquivo) {
		String linha = "";
		byte bb;
		try {
			while ((bb = (byte) arquivo.read()) != '\n') {
				linha += (char) bb;
			}
		} catch (IOException e) {
		}
	
		return linha;
	}
    //==========================================================================
    
    public BufferedImage showImage(String nome) throws Exception {
 
                        
            FileInputStream arquivo = new FileInputStream(nome);
            BufferedImage imagem_pgm = null;
		    int width = 0;
		    int height = 0;
		    int maxVal = 0;
			int count = 0;
			byte bb;
			
			String linha = MostrarImagemPGM.le_linha(arquivo);
	
				linha = MostrarImagemPGM.le_linha(arquivo);
				while (linha.startsWith("#")) {
					linha = MostrarImagemPGM.le_linha(arquivo);
				}
			    Scanner in = new Scanner(linha); 
			    if(in.hasNext() && in.hasNextInt())
			    	width = in.nextInt();
			    else
			    	System.out.println("Arquivo corrompido");
			    if(in.hasNext() && in.hasNextInt())
			    	height = in.nextInt();
			    else
			    	System.out.println("Arquivo corrompido");
                            
				linha = MostrarImagemPGM.le_linha(arquivo);
				in.close();
				in = new Scanner(linha);
				maxVal = in.nextInt();
				in.close();
                                
				imagem_pgm = new BufferedImage(width, height, BufferedImage.TYPE_BYTE_GRAY);
				byte [] pixels = ((DataBufferByte) imagem_pgm.getRaster().getDataBuffer()).getData();
				while(count < (height*width)) {
					bb = (byte) arquivo.read();
					pixels[count] = bb;	
					count++;
				}				
			
			arquivo.close();
                   
                         return imagem_pgm;
                        
                }
                        
       }


