
package Menus;

import classes.PGM;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;


/**
 *
 * @author joaopaulo
 */
public class MenuPGM extends javax.swing.JFrame {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
     * Creates new form MenuPGM
     */
    public MenuPGM() {
        initComponents();
    }


                           
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        aplicaFiltroBlur = new javax.swing.JButton();
        aplicaFiltroMensagem = new javax.swing.JButton();
        aplicaFiltroSharpen = new javax.swing.JButton();
        aplicaFiltroNegativo = new javax.swing.JButton();
        botaoLimparImagem = new javax.swing.JButton();
        localImagem = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        voltarMenuPrincipal = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        voltarMenuPPM = new javax.swing.JMenuItem();
        fecharEP2 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("MenuPGM");
        setExtendedState(6);
        setFocusCycleRoot(false);
        setFont(new java.awt.Font("Abyssinica SIL", 0, 10)); // NOI18N
        setResizable(false);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Image Filters"));

        aplicaFiltroBlur.setText("Blur");
        aplicaFiltroBlur.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                aplicaFiltroBlurActionPerformed(evt);
            }
        });

        aplicaFiltroMensagem.setText("Mensagem");
        aplicaFiltroMensagem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                aplicaFiltroMensagemActionPerformed(evt);
            }
        });

        aplicaFiltroSharpen.setText("Sharpen");
        aplicaFiltroSharpen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                aplicaFiltroSharpenActionPerformed(evt);
            }
        });

        aplicaFiltroNegativo.setText("Negativo");
        aplicaFiltroNegativo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                aplicaFiltroNegativoActionPerformed(evt);
            }
        });

        botaoLimparImagem.setText("Limpar Imagem");
        botaoLimparImagem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botaoLimparImagemActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(aplicaFiltroBlur, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(aplicaFiltroNegativo, javax.swing.GroupLayout.DEFAULT_SIZE, 277, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(aplicaFiltroSharpen, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(aplicaFiltroMensagem, javax.swing.GroupLayout.DEFAULT_SIZE, 284, Short.MAX_VALUE))
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(133, 133, 133)
                .addComponent(botaoLimparImagem, javax.swing.GroupLayout.PREFERRED_SIZE, 427, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 132, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(aplicaFiltroBlur)
                    .addComponent(aplicaFiltroSharpen))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(aplicaFiltroNegativo)
                    .addComponent(aplicaFiltroMensagem))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(botaoLimparImagem)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel1.setText("Imagem com Filtro");

        voltarMenuPrincipal.setText("Menu");

        jMenuItem1.setText("Menu Principal");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        voltarMenuPrincipal.add(jMenuItem1);

        voltarMenuPPM.setText("Menu PPM");
        voltarMenuPPM.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                voltarMenuPPMActionPerformed(evt);
            }
        });
        voltarMenuPrincipal.add(voltarMenuPPM);

        fecharEP2.setText("Fechar EP2");
        fecharEP2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fecharEP2ActionPerformed(evt);
            }
        });
        voltarMenuPrincipal.add(fecharEP2);

        jMenuBar1.add(voltarMenuPrincipal);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(66, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(64, 64, 64))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(162, 162, 162)
                        .addComponent(localImagem, javax.swing.GroupLayout.PREFERRED_SIZE, 500, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(104, 104, 104)
                        .addComponent(jLabel1)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 14, Short.MAX_VALUE)
                .addComponent(localImagem, javax.swing.GroupLayout.PREFERRED_SIZE, 400, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(17, 17, 17))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>                        

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {                                           
                 new MenuPrincipal().setVisible(true);
                 dispose();
       
    }                                          

    private void aplicaFiltroNegativoActionPerformed(java.awt.event.ActionEvent evt) {                                                     
            PGM aplicaNegativo = new PGM();
            MostrarImagemPGM pgm = new MostrarImagemPGM();
            String nome ;
        try {
            aplicaNegativo.aplicaFiltroNegativo();
            nome = aplicaNegativo.getArquivoNome();
 
            localImagem.setIcon(new ImageIcon(pgm.showImage(nome).getScaledInstance(500, 400, 100)));      
        } catch (IOException ex) {
            Logger.getLogger(MenuPGM.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(MenuPGM.class.getName()).log(Level.SEVERE, null, ex);
        }
      
            
    }                                                    

    private void aplicaFiltroMensagemActionPerformed(java.awt.event.ActionEvent evt) {                                                     
        try {
            PGM aplica = new PGM();
            aplica.decifraMensagemImagem();
            
        } catch (IOException ex) {
            Logger.getLogger(MenuPGM.class.getName()).log(Level.SEVERE, null, ex);
        }
    }                                                    

    private void botaoLimparImagemActionPerformed(java.awt.event.ActionEvent evt) {                                                  
                localImagem.setIcon(null);
    }                                                 

    private void aplicaFiltroBlurActionPerformed(java.awt.event.ActionEvent evt) {                                                 
         PGM aplicaNegativo = new PGM();
            MostrarImagemPGM pgm = new MostrarImagemPGM();
            String nome ;
        try {
            aplicaNegativo.aplicaFiltroBlur();
            nome = aplicaNegativo.getArquivoNome();
            localImagem.setIcon(new ImageIcon(pgm.showImage(nome).getScaledInstance(500, 400, 100)));      
        } catch (IOException ex) {
            Logger.getLogger(MenuPGM.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(MenuPGM.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }                                                

    private void voltarMenuPPMActionPerformed(java.awt.event.ActionEvent evt) {                                              
                        new MenuPPM().setVisible(true);
                        dispose();
    }                                             

    private void fecharEP2ActionPerformed(java.awt.event.ActionEvent evt) {                                          
                   System.exit(0);
    }                                         

    private void aplicaFiltroSharpenActionPerformed(java.awt.event.ActionEvent evt) {                                                    
                            PGM aplicaNegativo = new PGM();
            MostrarImagemPGM pgm = new MostrarImagemPGM();
            String nome ;
        try {
            aplicaNegativo.aplicaFiltroSharpen();
            nome = aplicaNegativo.getArquivoNome();
           
            localImagem.setIcon(new ImageIcon(pgm.showImage(nome).getScaledInstance(500, 400, 100)));      
        } catch (IOException ex) {
            Logger.getLogger(MenuPGM.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(MenuPGM.class.getName()).log(Level.SEVERE, null, ex);
        }
                        
    }                                                   

    // Variables declaration - do not modify                     
    private javax.swing.JButton aplicaFiltroBlur;
    private javax.swing.JButton aplicaFiltroMensagem;
    private javax.swing.JButton aplicaFiltroNegativo;
    private javax.swing.JButton aplicaFiltroSharpen;
    private javax.swing.JButton botaoLimparImagem;
    private javax.swing.JMenuItem fecharEP2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel localImagem;
    private javax.swing.JMenuItem voltarMenuPPM;
    private javax.swing.JMenu voltarMenuPrincipal;
    // End of variables declaration                   
}
