package Menus;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Scanner;

/**
 *
 * @author joaopaulo
 */
public class MostrarImagemPPM {
  //==========================================================================
    public static String le_linha(FileInputStream arquivo) {
		String linha = "";
		byte bb;
		try {
			while ((bb = (byte) arquivo.read()) != '\n') {
				linha += (char) bb;
			}
		} catch (IOException e) {
		}
		//System.out.println("Linha: " + linha);
		return linha;
	}
    //==========================================================================
    
    public BufferedImage showImage(String nome) throws Exception {
                    //String nome="";
               
                    FileInputStream arquivo = new FileInputStream(nome);
                    BufferedImage imagem_ppm = null;
                        int width = 0;
                        int height = 0;
                        int maxVal = 0;
			int count = 0;
			byte bb;
			
			String linha = MostrarImagemPPM.le_linha(arquivo);
	
				linha = MostrarImagemPPM.le_linha(arquivo);
				while (linha.startsWith("#")) {
					linha = MostrarImagemPPM.le_linha(arquivo);
				}
			    Scanner in = new Scanner(linha); 
			    if(in.hasNext() && in.hasNextInt())
			    	width = in.nextInt();
			    else
			    	System.out.println("Arquivo corrompido");
			    if(in.hasNext() && in.hasNextInt())
			    	height = in.nextInt();
			    else
			    	System.out.println("Arquivo corrompido");
                            
				linha = MostrarImagemPGM.le_linha(arquivo);
				in.close();
				in = new Scanner(linha);
				maxVal = in.nextInt();
				in.close();
                                
				imagem_ppm = new BufferedImage(width, height, BufferedImage.TYPE_3BYTE_BGR);
				byte [] pixels = ((DataBufferByte) imagem_ppm.getRaster().getDataBuffer()).getData();
				while(count < (height*width*3)) {
					bb = (byte) arquivo.read();
					pixels[count+2] = bb;	
					count++;
                                        bb = (byte) arquivo.read();
					pixels[count] = bb;
                                        count++;
                                        bb = (byte) arquivo.read();
					pixels[count-2] = bb;
                                        count++;
				}				
			arquivo.close();  
                         return imagem_ppm;
                        
                }                        
            }