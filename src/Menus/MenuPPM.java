
package Menus;

import classes.PPM;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;

/**
 *
 * @author joaopaulo
 */
public class MenuPPM extends javax.swing.JFrame {


	private static final long serialVersionUID = 1L;

    public MenuPPM() {
        initComponents();
    }
                       
    private void initComponents() {

        jMenu1 = new javax.swing.JMenu();
        jButton2 = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        filtroVermelho = new javax.swing.JButton();
        filtroAzul = new javax.swing.JButton();
        filtroVerde = new javax.swing.JButton();
        fitroNegativo = new javax.swing.JButton();
        botaoLimparImagem = new javax.swing.JButton();
        localImagem = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        menu_MenuPrincipal = new javax.swing.JMenu();
        voltarMenuPrincipal = new javax.swing.JMenuItem();
        voltarMenuPGM = new javax.swing.JMenuItem();
        menu_FecharEP2 = new javax.swing.JMenuItem();

        jMenu1.setText("jMenu1");

        jButton2.setText("jButton2");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("MenuPPM");
        setResizable(false);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Filtros"));

        filtroVermelho.setText("Vermelho");
        filtroVermelho.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                filtroVermelhoActionPerformed(evt);
            }
        });

        filtroAzul.setText("Azul");
        filtroAzul.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                filtroAzulActionPerformed(evt);
            }
        });

        filtroVerde.setText("Verde");
        filtroVerde.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                filtroVerdeActionPerformed(evt);
            }
        });

        fitroNegativo.setText("Negativo");
        fitroNegativo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fitroNegativoActionPerformed(evt);
            }
        });

        botaoLimparImagem.setText("Limpar Imagem");
        botaoLimparImagem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botaoLimparImagemActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(filtroVermelho, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(filtroAzul, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(30, 30, 30)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(fitroNegativo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(filtroVerde, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(141, 141, 141)
                .addComponent(botaoLimparImagem, javax.swing.GroupLayout.PREFERRED_SIZE, 349, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(202, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(filtroVermelho)
                    .addComponent(filtroVerde))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(filtroAzul)
                    .addComponent(fitroNegativo))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(botaoLimparImagem))
        );

        jLabel2.setText("Imagem com Filtro Aplicado");

        menu_MenuPrincipal.setText("Menu");

        voltarMenuPrincipal.setText("Menu Principal");
        voltarMenuPrincipal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                voltarMenuPrincipalActionPerformed(evt);
            }
        });
        menu_MenuPrincipal.add(voltarMenuPrincipal);

        voltarMenuPGM.setText("Menu PGM");
        voltarMenuPGM.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                voltarMenuPGMActionPerformed(evt);
            }
        });
        menu_MenuPrincipal.add(voltarMenuPGM);

        menu_FecharEP2.setText("Fechar EP2");
        menu_FecharEP2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menu_FecharEP2ActionPerformed(evt);
            }
        });
        menu_MenuPrincipal.add(menu_FecharEP2);

        jMenuBar1.add(menu_MenuPrincipal);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(49, 49, 49)
                        .addComponent(jLabel2))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(165, 165, 165)
                        .addComponent(localImagem, javax.swing.GroupLayout.PREFERRED_SIZE, 500, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 62, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(68, 68, 68))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel2)
                .addGap(18, 18, 18)
                .addComponent(localImagem, javax.swing.GroupLayout.PREFERRED_SIZE, 400, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(7, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>                        

    private void botaoLimparImagemActionPerformed(java.awt.event.ActionEvent evt) {                                                  
                    localImagem.setIcon(null);
    }                                                 

    private void filtroVermelhoActionPerformed(java.awt.event.ActionEvent evt) {                                               
                String nome;
                PPM aplica = new PPM();
                MostrarImagemPPM ppm = new MostrarImagemPPM();
        try {
            aplica.aplicafiltrovermelho();
            nome=aplica.getArquivoNome();
            localImagem.setIcon(new ImageIcon(ppm.showImage(nome).getScaledInstance(500, 400, 100)));
        } catch (IOException ex) {
            Logger.getLogger(MenuPPM.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(MenuPPM.class.getName()).log(Level.SEVERE, null, ex);
        }
    }                                              

    private void filtroAzulActionPerformed(java.awt.event.ActionEvent evt) {                                           
              String nome; 
               PPM aplica = new PPM();
               MostrarImagemPPM ppm = new MostrarImagemPPM();
        try {
            aplica.aplicafiltroazul();
            nome=aplica.getArquivoNome();
            localImagem.setIcon(new ImageIcon(ppm.showImage(nome).getScaledInstance(500, 400, 100)));
        } catch (IOException ex) {
            Logger.getLogger(MenuPPM.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(MenuPPM.class.getName()).log(Level.SEVERE, null, ex);
        }
    }                                          

    private void filtroVerdeActionPerformed(java.awt.event.ActionEvent evt) {                                            
                String nome;
                PPM aplica = new PPM();
                MostrarImagemPPM ppm = new MostrarImagemPPM();
        try {
            aplica.aplicafiltroverde();
            nome=aplica.getArquivoNome();
            localImagem.setIcon(new ImageIcon(ppm.showImage(nome).getScaledInstance(500, 400, 100)));
        } catch (IOException ex) {
            Logger.getLogger(MenuPPM.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(MenuPPM.class.getName()).log(Level.SEVERE, null, ex);
        }
    }                                           

    private void fitroNegativoActionPerformed(java.awt.event.ActionEvent evt) {                                              
                 String nome;
                PPM aplica = new PPM();
                MostrarImagemPPM ppm = new MostrarImagemPPM();
        try {
            aplica.aplicaFiltroNegativo();
            nome=aplica.getArquivoNome();
            localImagem.setIcon(new ImageIcon(ppm.showImage(nome).getScaledInstance(500, 400, 100)));
        } catch (IOException ex) {
            Logger.getLogger(MenuPPM.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(MenuPPM.class.getName()).log(Level.SEVERE, null, ex);
        }
    }                                             

    private void voltarMenuPrincipalActionPerformed(java.awt.event.ActionEvent evt) {                                                    
                 new MenuPrincipal().setVisible(true);
                 dispose();
    }                                                   

    private void voltarMenuPGMActionPerformed(java.awt.event.ActionEvent evt) {                                              
                        new MenuPGM().setVisible(true);
                        dispose();
    }                                             

    private void menu_FecharEP2ActionPerformed(java.awt.event.ActionEvent evt) {                                               
                        System.exit(0);
    }                                              



    // Variables declaration - do not modify                     
    private javax.swing.JButton botaoLimparImagem;
    private javax.swing.JButton filtroAzul;
    private javax.swing.JButton filtroVerde;
    private javax.swing.JButton filtroVermelho;
    private javax.swing.JButton fitroNegativo;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel localImagem;
    private javax.swing.JMenuItem menu_FecharEP2;
    private javax.swing.JMenu menu_MenuPrincipal;
    private javax.swing.JMenuItem voltarMenuPGM;
    private javax.swing.JMenuItem voltarMenuPrincipal;
    // End of variables declaration                   
}
