
package classes;


import java.io.IOException;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.File;
import java.io.FileOutputStream;
import java.io.RandomAccessFile;
import javax.swing.JOptionPane;

public class PGM extends Imagem{
	private int posicaoMensagem;
        private static String arquivofiltro;
        
    public PGM() {
        this.posicaoMensagem = 0;
         PGM.arquivofiltro = "";
    }
//===================================================================================================

    /**
     *
     * @return
     */
     public  String getArquivoNome() {
        return arquivofiltro;
    }

    public  void setArquivoNome(String arquivofiltro) {
        PGM.arquivofiltro = arquivofiltro;
    }
 
  
    public int getPosicaoMensagem() {
        return posicaoMensagem;
    }

    public void setPosicaoMensagem(int posicaoMensagem) {
        this.posicaoMensagem = posicaoMensagem;
    }
    //==================================================================================================
     
	   public void pegarPosicaoMensagem() throws IOException{
                    abrirArquivo();
	            int caracter=getArquivo().read();
	            String posicaoMensagem="";
	            while (caracter != -1) {
	                if ((char) caracter == '#') {
	                    caracter=getArquivo().read();
	                    caracter=getArquivo().read();
	                        while((char)caracter!=' '){
	                            posicaoMensagem+=(char)caracter;
	                            caracter=getArquivo().read();
	                        }
	                        setPosicaoMensagem(Integer.parseInt(posicaoMensagem));
	                    break;
	                }                
	             
	                caracter = getArquivo().read();
	        }
	        fecharArquivo();
	    }
	   
	    public void pegarPixels() throws IOException {
	    	
	        BufferedImage imagem = new BufferedImage(getAltura(), getLargura(), BufferedImage.TYPE_BYTE_GRAY);
	
	        byte[] pixels = ((DataBufferByte) (imagem).getRaster().getDataBuffer()).getData();
	        int contador = 0;
	        int caracter = getArquivo().read();
	
                    while (caracter != -1) {
                        contador++;
                        if(contador==getAltura()*getLargura()){

                        }
                        else{
                        pixels[contador] = (byte) caracter;
                    }
                        caracter = getArquivo().read();
                    }

                    setPixels(pixels);

                }
	    
	    public void decifrarImagem(){
	        int contador1=0;
	        int pixel=0,caracter=0;
	        String mensagem="";
	          for(int contador=getPosicaoMensagem()+2; contador1<8 ;contador++){
	            caracter= pixel|(caracter<<1);
	            pixel=((int)getPixels()[contador])&0x001;
	            if(contador1==7){
	                if((char)caracter!='#'){
	                    mensagem+=(char)caracter;
	                }
	                if(caracter!='#'||contador<getPosicaoMensagem()+16){
	                    contador1=-1;
	                    caracter=0;
	                }
	            }
	            contador1++;
	          }
                  
                  if(mensagem.length()>200){
                        int tamanho = 0;
                        String aux1="";
                        String aux2=mensagem;
                        while (tamanho<aux2.length()) {
                            aux1+="\n"+(aux2.substring(tamanho, Math.min(tamanho+200,aux2.length())));
                            tamanho+=200;
                        }
                        JOptionPane.showMessageDialog(null, aux1);
                    }else{
                        JOptionPane.showMessageDialog(null,mensagem);
                    }
	          
	    }
            
            public void applyNegativeFilter() throws IOException{
        
                String nome="";
	        File file = new File("NEGATIVEFilter.pgm");
                    try (FileOutputStream temp = new FileOutputStream(file)) {
                        if (!file.exists()) {
                            file.createNewFile();
                        }
                        temp.write(getChaveImagem().getBytes());
                        temp.write('\n');
                        temp.write(Integer.toString(getAltura()).getBytes());
                        temp.write(' ');
                        temp.write(Integer.toString(getLargura()).getBytes());
                        temp.write('\n');
                        temp.write(Integer.toString(getPixelMaximo()).getBytes());
                        temp.write('\n');
                        for (int contador = 0; contador < getPixels().length; contador++) {
                           
                                temp.write(getPixelMaximo()-getPixels()[contador]);
                                         
                        }   
                    }
                    JOptionPane.showMessageDialog(null,"Filtro Negativo Aplicado\nImagem salva em :"+file.getAbsolutePath());
                    nome = file.getAbsolutePath();
                    setArquivoNome(nome);
            }
            
//=====================================================================================================================================  
            public void applyBlurFilter() throws IOException{   
                File arquivo = new File(getNomeArquivo());
	        RandomAccessFile aberturaDoArquivo = null;
                aberturaDoArquivo = new RandomAccessFile(arquivo, "r");
                char[] pixels = new char[getAltura()*getLargura()];            
                String nome;
        
                //char[] caracter = new char[1000];
               // int cont1=0,cont2=0;
                 
                    for(int i = 0; i < pixels.length; i++){
                        pixels[i] = (char) aberturaDoArquivo.read();
                        if(i==0){
                       
                        }
                    }
                    
                    char[] imagem = new char[pixels.length];
                   
                    for (int i = 0; i < 3; i++) {
                        for (int j = 0; j < getLargura(); j++) {
                            imagem[i + j * getLargura()] = (char) pixels[i + j * getLargura()];
                        }
                    }
                    for (int contador1 = 0; contador1 < 3; contador1++) {
                         for (int contador2 = 0; contador2 < getAltura(); contador2++) {
                            imagem[contador1 + contador2 * getLargura()] = (char) pixels[contador1 + contador2 * getLargura()];
                        }
                    }
           
                    int div=9;
                    int size = 3;
                    int limit =1;
                    int valor;
                    int[] filtro={1, 1, 1, 1, 1, 1, 1, 1, 1};
          
                    File file = new File("BLURFilter.pgm");
                    try (FileOutputStream temp = new FileOutputStream(file)) {
                        if (!file.exists()) {
                            file.createNewFile();
                        }
                        temp.write(getChaveImagem().getBytes());
                        temp.write('\n');
                        temp.write(Integer.toString(getAltura()).getBytes());
                        temp.write(' ');
                        temp.write(Integer.toString(getLargura()).getBytes());
                        temp.write('\n');
                        temp.write(Integer.toString(getPixelMaximo()).getBytes());
                        temp.write('\n');
                        for (int i = limit; i < getLargura() - limit; i++) {
                            for (int j = limit; j < getAltura() - limit; j++) {
                                valor = 0;
                                for (int x = -1; x <= 1; x++) {
                                    for (int y = -1; y <= 1; y++) {
                                        valor += filtro[(x + 1) + size * (y + 1)] * pixels[(i + x) + (y + j) * getLargura()];
                                    }
                                }
                            valor /= div;
                            valor = valor < 0 ? 0 : valor;
                            valor = valor > getPixelMaximo()? getPixelMaximo(): valor;
                            imagem[i + j * getLargura()] = (char) valor;
                            temp.write( valor);
                            }
                        }
                          
                    }
                      //=======================================================================================
                    JOptionPane.showMessageDialog(null,"Filtro BLUR Aplicado\nImagem salva em :"+file.getAbsolutePath());
                    nome = file.getAbsolutePath();
                    setArquivoNome(nome);
                    aberturaDoArquivo.close();
            }
//=====================================================================================================================================
            public void applySharpenFilter() throws IOException{   
                File arquivo = new File(getNomeArquivo());
	        RandomAccessFile aberturaDoArquivo = null;
                aberturaDoArquivo = new RandomAccessFile(arquivo, "r");
                char[] pixels = new char[getAltura()*getLargura()];            
                String nome;
        
                //char[] caracter = new char[1000];
                //int cont1=0,cont2=0;
                  
                    for(int i = 0; i < pixels.length; i++){
                        pixels[i] = (char) aberturaDoArquivo.read();
                        if(i==0){
                        
                        }
                    }
                    
                    char[] imagem = new char[pixels.length];
                   
                    for (int i = 0; i < 3; i++) {
                        for (int j = 0; j < getLargura(); j++) {
                            imagem[i + j * getLargura()] = (char) pixels[i + j * getLargura()];
                        }
                    }
                    for (int contador1 = 0; contador1 < 3; contador1++) {
                         for (int contador2 = 0; contador2 < getAltura(); contador2++) {
                            imagem[contador1 + contador2 * getLargura()] = (char) pixels[contador1 + contador2 * getLargura()];
                        }
                    }
           
                    int div=1;
                    int size = 3;
                    int limit =1;
                    int valor;
                    int[] filtro={0, -1, 0, -1, 5, -1, 0, -1, 0};
          
                    File file = new File("SHARPENFilter.pgm");
                    try (FileOutputStream temp = new FileOutputStream(file)) {
                        if (!file.exists()) {
                            file.createNewFile();
                        }
                        temp.write(getChaveImagem().getBytes());
                        temp.write('\n');
                        temp.write(Integer.toString(getAltura()).getBytes());
                        temp.write(' ');
                        temp.write(Integer.toString(getLargura()).getBytes());
                        temp.write('\n');
                        temp.write(Integer.toString(getPixelMaximo()).getBytes());
                        temp.write('\n');
                        for (int i = limit; i < getLargura() - limit; i++) {
                            for (int j = limit; j < getAltura() - limit; j++) {
                                valor = 0;
                                for (int x = -1; x <= 1; x++) {
                                    for (int y = -1; y <= 1; y++) {
                                        valor += filtro[(x + 1) + size * (y + 1)] * pixels[(i + x) + (y + j) * getLargura()];
                                    }
                                }
                            valor /= div;
                            valor = valor < 0 ? 0 : valor;
                            valor = valor > getPixelMaximo()? getPixelMaximo(): valor;
                            imagem[i + j * getLargura()] = (char) valor;
                            temp.write( valor);
                            }
                        }
                          
                    }
                      //=======================================================================================
                    JOptionPane.showMessageDialog(null,"Filtro SHARPEN Aplicado\nImagem salva em :"+file.getAbsolutePath());
                    nome = file.getAbsolutePath();
                    setArquivoNome(nome);
                    aberturaDoArquivo.close();
            }
//=============================================================================================================================
            public void validaTipoPGM(){
              
                 if(getChaveImagem().equals("P5")) {
                    JOptionPane.showMessageDialog(null,"Imagem do formato PGM inserida\n Aplicando filtro...");
                    }
                 else {
                    JOptionPane.showMessageDialog(null,"ERRO!\nFormato de imagem inválido!\nInsira uma imagem do formato PGM");              
                    return;                    
                    }
            
            }
            public void aplicaFiltroNegativo() throws IOException{
            PGM im = new PGM();
            im.pegarNomeArquivo(); 
            im.abrirArquivo();
            im.pegarChaveImagem();
            im.validaTipoPGM();
            im.pegarAltura();
            im.pegarLargura();
            im.pegarPixelMaximo();
            im.pegarPixels();
            im.fecharArquivo();
            im.applyNegativeFilter();
            }
            
            public void aplicaFiltroBlur() throws IOException{
             PGM im = new PGM();
            im.pegarNomeArquivo(); 
            im.abrirArquivo();
            im.pegarChaveImagem();
            im.validaTipoPGM();
            im.pegarAltura();
            im.pegarLargura();
            im.pegarPixelMaximo();
            im.applyBlurFilter();
            im.fecharArquivo();
            }
            public void aplicaFiltroSharpen() throws IOException{
             PGM im = new PGM();
            im.pegarNomeArquivo(); 
            im.abrirArquivo();
            im.pegarChaveImagem();
            im.validaTipoPGM();
            im.pegarAltura();
            im.pegarLargura();
            im.pegarPixelMaximo();
            im.applySharpenFilter();
            im.fecharArquivo();
            }
            
            public void decifraMensagemImagem() throws IOException{
                PGM im = new PGM();
            im.pegarNomeArquivo();
            im.abrirArquivo();
            im.pegarChaveImagem();
            im.validaTipoPGM();
            im.pegarAltura();
            im.pegarLargura();
            im.pegarPixelMaximo();
            im.pegarPixels();
            im.pegarPosicaoMensagem();
            im.fecharArquivo();
            im.decifrarImagem();
            }
	   
	}