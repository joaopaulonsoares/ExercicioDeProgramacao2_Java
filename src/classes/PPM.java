/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

/**
 *
 * @author joaopaulo
 */

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.*;
import javax.swing.*;


public class PPM extends Imagem{
    private static String arquivofiltro;
    
		public PPM(){
		PPM.arquivofiltro = "";
		}
                
    public  String getArquivoNome() {
        return arquivofiltro;
    }

    public  void setArquivoNome(String arquivofiltro) {
        PPM.arquivofiltro = arquivofiltro;
    }

	@Override
	 public void pegarPixels() throws IOException {
	        BufferedImage imagem = new BufferedImage(getAltura(), getLargura(), BufferedImage.TYPE_3BYTE_BGR);
	        byte[] pixels = ((DataBufferByte) imagem.getRaster().getDataBuffer()).getData();
	        int contador = 0;
	        int caracter = getArquivo().read();
	        while (caracter != -1) {
	            if (contador == getAltura() * getLargura() * 3 && (char) caracter == '\n') {
	            } else {
	                pixels[contador] = (byte) caracter;
	                contador++;
	            }
	            caracter = getArquivo().read();
	        }
	        setPixels(pixels);
	    }
	//======================APLICAR FILTROS IMAGEM PPM ===============================================
	 public void applyFilterBlue() throws FileNotFoundException, IOException {
		
	        int contador1 = 0;
	        File file = new File("BLUEFilter.ppm");
                    try (FileOutputStream temp = new FileOutputStream(file)) {
                        if (!file.exists()) {
                            file.createNewFile();
                        }
                        temp.write(getChaveImagem().getBytes());
                        temp.write('\n');
                        temp.write(Integer.toString(getAltura()).getBytes());
                        temp.write(' ');
                        temp.write(Integer.toString(getLargura()).getBytes());
                        temp.write('\n');
                        temp.write(Integer.toString(getPixelMaximo()).getBytes());
                        temp.write('\n');
                        for (int contador = 0; contador < getPixels().length; contador++) {
                            if (contador1 == 2) {
                                temp.write(getPixels()[contador]);
                                contador1=0;
                            } else {
                                temp.write((char) 0);
                                contador1++;
                            }
                        }   
                    }
                    
                    setArquivoNome(file.getAbsolutePath());
                         JOptionPane.showMessageDialog(null,"Filtro Azul Aplicado\nImagem salva em :"+file.getAbsolutePath());
	    }
	 public void applyFilterRed() throws FileNotFoundException, IOException {
	        int contador1 = 0;
	        File file = new File("REDFilter.ppm");
                    try (FileOutputStream temp = new FileOutputStream(file)) {
                        if (!file.exists()) {
                            file.createNewFile();
                        }
                        temp.write(getChaveImagem().getBytes());
                        temp.write('\n');
                        temp.write(Integer.toString(getAltura()).getBytes());
                        temp.write(' ');
                        temp.write(Integer.toString(getLargura()).getBytes());
                        temp.write('\n');
                        temp.write(Integer.toString(getPixelMaximo()).getBytes());
                        temp.write('\n');
                        for (int contador = 0; contador < getPixels().length; contador++) {
                            if (contador1 == 0) {
                                temp.write(getPixels()[contador]);
                                contador1++;
                            } else {
                                temp.write((byte) 0);
                                contador1++;
                                if (contador1 == 3) {
                                    contador1 = 0;
                                }
                            }
                        }   
                    }
                     setArquivoNome(file.getAbsolutePath());
                          JOptionPane.showMessageDialog(null,"Filtro Vermelho Aplicado\nImagem salva em :"+file.getAbsolutePath());
	    }
	 
	 public void applyFilterGreen() throws FileNotFoundException, IOException {
	        int contador1 = 0;
	        File file = new File("GREENFilter.ppm");
                    try (FileOutputStream temp = new FileOutputStream(file)) {
                        if (!file.exists()) {
                            file.createNewFile();
                        }
                        temp.write(getChaveImagem().getBytes());
                        temp.write('\n');
                        temp.write(Integer.toString(getAltura()).getBytes());
                        temp.write(' ');
                        temp.write(Integer.toString(getLargura()).getBytes());
                        temp.write('\n');
                        temp.write(Integer.toString(getPixelMaximo()).getBytes());
                        temp.write('\n');
                        for (int contador = 0; contador < getPixels().length; contador++) {
                            if (contador1 == 1) {
                                temp.write(getPixels()[contador]);
                                contador1++;
                            } else {
                                temp.write((char) 0);
                                contador1++;
                                if (contador1 == 3) {
                                    contador1 = 0;
                                }
                            }
                        }   
                    }
                     setArquivoNome(file.getAbsolutePath());
                    JOptionPane.showMessageDialog(null,"Filtro Verde Aplicado\nImagem salva em :"+file.getAbsolutePath());
	    }
          public void applyNegativeFilter() throws IOException{
            
                String nome="";
	        File file = new File("NEGATIVEFilter.ppm");
                    try (FileOutputStream temp = new FileOutputStream(file)) {
                        if (!file.exists()) {
                            file.createNewFile();
                        }
                        temp.write(getChaveImagem().getBytes());
                        temp.write('\n');
                        temp.write(Integer.toString(getAltura()).getBytes());
                        temp.write(' ');
                        temp.write(Integer.toString(getLargura()).getBytes());
                        temp.write('\n');
                        temp.write(Integer.toString(getPixelMaximo()).getBytes());
                        temp.write('\n');
                        for (int contador = 0; contador < getPixels().length; contador++) {
                           
                                temp.write(getPixelMaximo()-getPixels()[contador]);
                                         
                        }   
                    }
                    JOptionPane.showMessageDialog(null,"Filtro Negativo Aplicado\nImagem salva em :"+file.getAbsolutePath());
                    nome = file.getAbsolutePath();
                    setArquivoNome(nome);
            }
         public int validaTipoPPM(){
                
                 if(getChaveImagem().equals("P6")) {
                    
                    return 10; 
                }else {      
                    return 5;
                    }
				   
            }
	 
	
//=============================================================================================
	
//================================================================================================
         public void aplicaFiltroNegativo() throws IOException{
            PPM im = new PPM();
            im.pegarNomeArquivo(); 
            im.abrirArquivo();
            im.pegarChaveImagem();
            im.validaTipoPPM();
            if(im.validaTipoPPM()!=10){
            	JOptionPane.showMessageDialog(null,"ERRO!\nFormato de imagem inválido!\nInsira uma imagem do formato PPM");
            	im.fecharArquivo();
            	return;
            }
            JOptionPane.showMessageDialog(null,"Imagem do formato PPM inserida\n Aplicando filtro...");
            im.pegarAltura();
            im.pegarLargura();
            im.pegarPixelMaximo();
            im.pegarPixels();
            im.fecharArquivo();
            im.applyNegativeFilter();
            }
         public void aplicafiltrovermelho() throws IOException{
              PPM im = new PPM();
            im.pegarNomeArquivo(); 
            im.abrirArquivo();
            im.pegarChaveImagem();
            im.validaTipoPPM();
            if(im.validaTipoPPM()!=10){
            	JOptionPane.showMessageDialog(null,"ERRO!\nFormato de imagem inválido!\nInsira uma imagem do formato PPM");
            	im.fecharArquivo();
            	return;
            }
            JOptionPane.showMessageDialog(null,"Imagem do formato PPM inserida\n Aplicando filtro...");
            im.pegarAltura();
            im.pegarLargura();
            im.pegarPixelMaximo();
            im.pegarPixels();
            im.fecharArquivo();
            im.applyFilterRed();
         }
          public void aplicafiltroverde() throws IOException{
            PPM im = new PPM();
            im.pegarNomeArquivo();
            im.abrirArquivo();
            im.pegarChaveImagem();
            im.validaTipoPPM();
            if(im.validaTipoPPM()!=10){
            	JOptionPane.showMessageDialog(null,"ERRO!\nFormato de imagem inválido!\nInsira uma imagem do formato PPM");
            	im.fecharArquivo();
            	return;
            }
            JOptionPane.showMessageDialog(null,"Imagem do formato PPM inserida\n Aplicando filtro...");
            im.pegarAltura();
            im.pegarLargura();
            im.pegarPixelMaximo();
            im.pegarPixels();
            im.fecharArquivo();
            im.applyFilterGreen();
          }
           public void aplicafiltroazul() throws IOException{
            PPM im = new PPM();
            im.pegarNomeArquivo();
            im.abrirArquivo();
            im.pegarChaveImagem();
            im.validaTipoPPM();
            if(im.validaTipoPPM()!=10){
            	JOptionPane.showMessageDialog(null,"ERRO!\nFormato de imagem inválido!\nInsira uma imagem do formato PPM");
            	im.fecharArquivo();
            	return;
            }
            JOptionPane.showMessageDialog(null,"Imagem do formato PPM inserida\n Aplicando filtro...");
            im.pegarAltura();
            im.pegarLargura();
            im.pegarPixelMaximo();
            im.pegarPixels();
            im.fecharArquivo();
            im.applyFilterBlue();
          }
}