package classes;

import java.io.*;
//import java.io.File;
//import java.io.FileInputStream;
//import java.io.FileNotFoundException;
//import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

public abstract class Imagem {
	
	private String chaveImagem;
	private int tamanho;
	private int largura;
	private int altura;
	private String nomeArquivo;
	private FileInputStream arquivo;
	private int pixelMaximo;
	protected byte[] pixels;
	
	
	public Imagem(){
		this.chaveImagem="";
		this.tamanho=0;
		this.largura=0;
		this.altura=0;
		this.nomeArquivo="";
		this.pixelMaximo=0;
	}
//===============================================================================

	public String getChaveImagem() {
		return chaveImagem;
	}
	public void setChaveImagem(String chaveImagem) {
		this.chaveImagem = chaveImagem;
	}


	public int getTamanho() {
		return tamanho;
	}
	public void setTamanho(int tamanho) {
		this.tamanho = tamanho;
	}


	public int getLargura() {
		return largura;
	}
	public void setLargura(int largura) {
		this.largura = largura;
	}


	public int getAltura() {
		return altura;
	}
	public void setAltura(int altura) {
		this.altura = altura;
	}


	public String getNomeArquivo() {
		return nomeArquivo;
	}
	public void setNomeArquivo(String nomeArquivo) {
		this.nomeArquivo = nomeArquivo;
	}


	public FileInputStream getArquivo() {
		return arquivo;
	}
	public void setArquivo(FileInputStream arquivo) {
		this.arquivo = arquivo;
	}


	public int getPixelMaximo() {
		return pixelMaximo;
	}
	public void setPixelMaximo(int pixelMaximo) {
		this.pixelMaximo = pixelMaximo;
	}
	
	public byte[] getPixels() {
	    return pixels;
	}
	public void setPixels(byte[] pixel) {
	    this.pixels = pixel;
	}
	
//===============================================================================	
	 public void pegarAltura() throws IOException {
	        int contador = 1, contador1 = 0, caracter = getArquivo().read();
	        String altura = "";
	        while (caracter != -1) {
	            if ((char) caracter == '#') {
	                contador1--;
	            }
	            if ((char) caracter == '\n') {
	                contador1++;
	                caracter = getArquivo().read();
	                contador++;
	                if ((char) caracter == '#') {
	                    contador1--;
	                    contador++;
	                }
	            }
	            if (contador1 == 0) {
	                while (caracter != ' ') {
	                    altura += (char) caracter;
	                    contador++;
	                    caracter = getArquivo().read();
	                }
	                setAltura(Integer.parseInt(altura));
	                break;
	            }
	            contador++;
	            caracter = getArquivo().read();
	        }
	        setTamanho(getTamanho() + contador);
	        //System.out.println("Altura: "+altura);
	    }

	    public void pegarLargura() throws IOException {
	        int contador = 1, contador1 = 0, caracter = getArquivo().read();
	        String largura = "";
	        while (caracter != -1) {
	            if ((char) caracter == '#') {
	                contador1--;
	                contador++;
	            }
	            if ((char) caracter == '\n') {
	                contador1++;
	                caracter = getArquivo().read();
	                if ((char) caracter == '#') {
	                    contador1--;
	                    contador++;
	                }
	            }
	            if (contador1 == 0) {
	                while (caracter != '\n') {
	                    largura += (char) caracter;
	                    caracter = getArquivo().read();
	                    contador++;
	                }
	                setLargura(Integer.parseInt(largura));
	                break;
	            }
	            contador++;
	            caracter = getArquivo().read();
	        }
	        setTamanho(getTamanho() + contador);
	        //System.out.println("Largura: "+largura);
	    }

	    public void pegarPixelMaximo() throws IOException {
	        int contador = 1, contador1 = 0, caracter = getArquivo().read();
	        String valorMaximo = "";
	        while (caracter != -1) {
	            if ((char) caracter == '#') {
	                contador1--;
	            }
	            if ((char) caracter == '\n') {
	                contador1++;
	                caracter = getArquivo().read();
	                contador++;
	                if ((char) caracter == '#') {
	                    contador1--;
	                    contador++;
	                }
	            }
	            if (contador1 == 0) {
	                while (caracter != '\n') {
	                    valorMaximo += (char) caracter;
	                    contador++;
	                    caracter = getArquivo().read();
	                }
	                setPixelMaximo(Integer.parseInt(valorMaximo));
	                break;
	            }
	            contador++;
	            caracter = getArquivo().read();
	        }
	        setTamanho(getTamanho() + contador);
	       // System.out.println("Pixel: "+valorMaximo);
	    }

	
	public void pegarNomeArquivo() throws FileNotFoundException {
		String nome;
		JFileChooser chooser = new JFileChooser();
		 FileNameExtensionFilter filterppm = new FileNameExtensionFilter("PPM", "ppm");
         FileNameExtensionFilter filterpgm = new FileNameExtensionFilter("PGM", "pgm");
         
         chooser.setAcceptAllFileFilterUsed(false);
         chooser.setFileFilter(filterppm);
         chooser.setFileFilter(filterpgm);
    	int option = chooser.showOpenDialog(null);
    	if (option == JFileChooser.APPROVE_OPTION) {  
    	 java.io.File f = chooser.getSelectedFile();  
    	 nome = f.getAbsolutePath();
    	 setNomeArquivo (nome);
    	 }
    }
	
	public void abrirArquivo() throws FileNotFoundException {
        setArquivo(new FileInputStream(getNomeArquivo()));
    }
	 public void fecharArquivo() throws IOException {
	    getArquivo().close();
	}
	 
	 public void pegarChaveImagem() throws IOException {
	        int contador = 1, contador1 = 0;
	       
	        int caracter = getArquivo().read();
	        while (caracter != -1) {
	            if ((char) caracter == '#') {
	                contador1--;
	                contador++;
	            }
	            if ((char) caracter == '\n') {
	                contador1++;
	                caracter = getArquivo().read();
	                if ((char) caracter == '#') {
	                    contador1--;
	                    contador++;
	                }
	            }
	            if (contador1 == 0) {
	                while (caracter != '\n') {
	                    setChaveImagem(getChaveImagem() + (char) caracter);
	                    caracter = getArquivo().read();
	                    contador++;
	                }
	                break;
	            }
	            contador++;
	            caracter = getArquivo().read();
	        }
	        setTamanho(contador);
	      //  System.out.println("A chave da Imagem e: "+getChaveImagem());
	        
	  }

	    public abstract void pegarPixels() throws IOException;


}
