===============================PASTAS DO PROGRAMA==========================================================

src: Pasta que contém pacotes com arquivos .java;
dist:Pasta que contém o arquivo executável do programa(.jar);

===============================INFORMAÇÕES GERAIS==========================================================

O programa foi feito em java utilizando as IDEs NetBeans e Eclipse Mars.
Para execução do programa é necessário que haja a JVM, JRE.


===============================PASSOS PARA A EXECUÇÃO DO PROGRAMA===========================================

Após abrir o programa, seja por meio do executável ou importando-o para uma IDE, deve-se seguir os seguintes passos:

Primeiro - No Menu Principal escolha o tipo de imagem que deseja manipular.
		PGM--> Imagens do formato .pgm
		PPM--> Imagens do formato .ppm
Segundo -   Abrirá o respectivo Menu de cada formato.Escolha a ação que deseja realizar.Assim que o botão for acionado, você poderá escolher o arquivo que deseja manipular.

Terceiro -  Inserindo uma imagem do respectivo formato que deseja a ação será realizada.No caso dos filtros de imagem as imagens serão salvas e o diretório informado, logo após isso a imagem com filtro será mostrada na tela.
	     Já no caso da mensagem decifrada, uma tela será aberta mostrando a mensagem que estava escondida.

Quarto - 

Quinto - Repita o processo caso queira continuar.

Sexto - No Menu localizado na parte superior do programa você poderá transitar entre o MenuPPM e o MenuPGM.




